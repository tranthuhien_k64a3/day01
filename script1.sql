create database QLSV;
use QLSV;

create table DMKHOA (
    MaKH varchar(6) not null,
    TenKhoa varchar(30),
    primary key(MaKH)
);

create table SINHVIEN(
    MaSV varchar(6) not null,
    HoSV varchar(30),
    TenSV varchar(15),
    GioiTinh char(1),
    NgaySinh datetime,
    NoiSinh varchar(50),
    DiaChi varchar(50),
    MaKH varchar(6),
    HocBong int,
    primary key(MaSV)
);


